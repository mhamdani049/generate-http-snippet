var HTTPSnippet = require("httpsnippet");

var snippet = new HTTPSnippet({
  method: "POST",
  url: "http://session-api.amanahapp.run",
  httpVersion: "HTTP/1.1",
  cookies: [],
  headers: [
    {
      name: "Content-Type",
      value: "application/json",
      comment: "",
    },
  ],
  postData: {
    mimeType: "application/x-www-form-urlencoded",
    params: [
      {
        name: "email",
        value: "rengganissamudera@gmail.com",
      },
      {
        name: "password",
        value: "123456",
      },
    ],
  },
  headersSize: 150,
  bodySize: 0,
  comment: "",
});

console.log(snippet.convert("go"));
